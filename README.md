## wuys-ops/runner

A docker compose file that help deploy runner much easier

### Deploy runner

Remember to modify `compose.yml` file, allow only runner you want to use.

```bash
git clone https://gitlab.com/wuys-ops/runner.git
cd runner.git
docker compose up -d
```

### Add runner to repo

#### Gitlab

```bash
docker exec -it gitlab-runner gitlab-runner register
```

```
Enter the GitLab instance URL (for example, https://gitlab.com/):
https://gitlab.com/
Enter the registration token:
xxxx
Enter a description for the runner:
[945e58e50826]: example
Enter tags for the runner (comma-separated):

Enter optional maintenance note for the runner:

WARNING: Support for registration tokens and runner parameters in the 'register' command has been deprecated in GitLab Runner 15.6 and will be replaced with support for authentication tokens. For more information, see https://docs.gitlab.com/ee/ci/runners/new_creation_workflow
Registering runner... succeeded                     runner=xxxx
Enter an executor: custom, docker-windows, ssh, instance, docker, parallels, shell, virtualbox, docker-autoscaler, docker+machine, kubernetes:
docker
Enter the default Docker image (for example, ruby:2.7):
docker:latest
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!

Configuration (with the authentication token) was saved in "/etc/gitlab-runner/config.toml"
```

Then follow registation steps, remember using `docker` as executor. After that, modify config of runner then it can use host Docker socket instead of DIND.

Open `./config/gitlab/config.toml` and modify this line

```bash
volumes = ["/cache"]
```

to

```bash
volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
pull_policy = "if-not-present"
```
